import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

export const fetchText = createAsyncThunk('text/fetchText', async function () {
  const data = fetch(
    'https://baconipsum.com/api/?type=meat-and-filler&paras=5&format=text'
  )
    .then((res) => res.text())
    .then((text) => text);

  return data;
});

export const textDataSlice = createSlice({
  name: 'text',
  initialState: {
    enteredText: '',
    textToBeEntered: '',
    errorText: false,
    status: null,
    error: null,
  },
  reducers: {
    textDataToInput: (state) => {
      state.textToBeEntered;
    },
    textDataCorrectSymbolAdded: (state) => {
      const arrTextToInput = state.textToBeEntered.split('');

      state.enteredText = state.enteredText + arrTextToInput[0];
      state.textToBeEntered = arrTextToInput.slice(1).join('');
      state.errorText = false;
    },
    textDataIncorrectSymbolAdded: (state) => {
      state.errorText = true;
    },
    textDataReset: (state) => {
      state.enteredText = '';
      state.textToBeEntered = '';
      return textDataSlice.initialState;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(fetchText.pending, (state) => {
      state.status = true;
    });
    builder.addCase(fetchText.fulfilled, (state, action) => {
      state.status = false;
      state.textToBeEntered = action.payload;
    });
    builder.addCase(fetchText.rejected, (state, action) => {
      if (action.payload) {
        state.error = action.payload.errorMessage;
        console.log(state.error);
      } else {
        state.error = action.error.message;
        console.log(state.error);
      }
    });
  },
});

export const {
  textDataToInput,
  textDataCorrectSymbolAdded,
  textDataIncorrectSymbolAdded,
  textDataReset,
} = textDataSlice.actions;

export default textDataSlice.reducer;
