import { createSlice } from '@reduxjs/toolkit';

export const userInputSlice = createSlice({
  name: 'user',
  initialState: {
    typesCount: 0,
    startTime: null,
  },
  reducers: {
    userInputKeyPresed: (state) => {
      state.typesCount = state.typesCount + 1;
      state.startTime = state.startTime
        ? state.startTime
        : new Date().getTime();
    },
    userInputReset: () => userInputSlice.initialState,
  },
});

export const { userInputKeyPresed, userInputReset } = userInputSlice.actions;

export default userInputSlice.reducer;
