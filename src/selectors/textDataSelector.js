export const textDataSelector = (state) => state.text;

export const textDataStatusSelector = (state) => textDataSelector(state).status;
export const textDataEnteredTextSelector = (state) =>
  textDataSelector(state).enteredText;
export const textDataTextToBeEnteredSelector = (state) =>
  textDataSelector(state).textToBeEntered;
export const textDataErrorSelector = (state) =>
  textDataSelector(state).errorText;
