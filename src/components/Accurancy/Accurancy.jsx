import { useSelector } from 'react-redux';
import { typesCountSelector } from '../../selectors/userInputSelectors';
import { textDataEnteredTextSelector } from '../../selectors/textDataSelector';
import { Card, Col, Statistic } from 'antd';

const useGetAccuracy = () => {
  const typesCount = useSelector(typesCountSelector);
  const enteredTextCount = useSelector(textDataEnteredTextSelector).length;

  if (typesCount === 0) {
    return 100;
  }

  return Math.floor((enteredTextCount / typesCount) * 100);
};

export const Accurancy = () => {
  const accuracy = useGetAccuracy();

  return (
    <Col span={12}>
      <Card bordered={false}>
        <Statistic
          title='Точность'
          value={accuracy}
          precision={2}
          valueStyle={{ color: '#3f8600' }}
          suffix='%'
        />
      </Card>
    </Col>
  );
};
