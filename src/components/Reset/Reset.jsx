import { useDispatch } from 'react-redux';
import { useCallback } from 'react';
import { RedoOutlined } from '@ant-design/icons';
import { userInputReset } from '../../slices/userInputSlice/userInputSlice';
import {
  fetchText,
  textDataReset,
} from '../../slices/textDataSlice/textDataSlice';
import { Button, Col } from 'antd';

const useReset = () => {
  const dispatch = useDispatch();

  return useCallback(
    (e) => {
      e.target.blur();

      dispatch(userInputReset());
      dispatch(textDataReset());
      dispatch(fetchText());
    },
    [dispatch, fetchText]
  );
};

export const Reset = () => {
  const handleClick = useReset();

  return (
    <Col span={12} style={{ marginTop: '16px' }}>
      <Button
        onClick={handleClick}
        type='primary'
        icon={<RedoOutlined />}
        size={'large'}
      >
        Заново
      </Button>
    </Col>
  );
};
