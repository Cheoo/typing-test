import { useDispatch, useSelector } from 'react-redux';
import {
  textDataEnteredTextSelector,
  textDataErrorSelector,
  textDataStatusSelector,
  textDataTextToBeEnteredSelector,
} from '../../selectors/textDataSelector';
import {
  fetchText,
  textDataCorrectSymbolAdded,
  textDataIncorrectSymbolAdded,
} from '../../slices/textDataSlice/textDataSlice';
import { useEffect } from 'react';
import { Card, Spin, Typography } from 'antd';
import { userInputKeyPresed } from '../../slices/userInputSlice/userInputSlice';
const Paragraph = Typography;

const useHandleUserInput = (textToBeEntered) => {
  const dispatch = useDispatch();

  const handleUserInput = (e) => {
    const targetSymbol = textToBeEntered[0];
    dispatch(userInputKeyPresed());

    if (e.key === targetSymbol) {
      dispatch(textDataCorrectSymbolAdded());
    } else {
      dispatch(textDataIncorrectSymbolAdded());
    }
  };

  useEffect(() => {
    window.addEventListener('keypress', handleUserInput);

    return () => {
      window.removeEventListener('keypress', handleUserInput);
    };
  }, [handleUserInput]);
};

export const Text = () => {
  const textToBeEntered = useSelector(textDataTextToBeEnteredSelector);
  const enteredText = useSelector(textDataEnteredTextSelector);
  const status = useSelector(textDataStatusSelector);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchText());
  }, [dispatch]);

  const [firstLetter, ...othersLetters] = textToBeEntered;

  useHandleUserInput(textToBeEntered);
  const error = useSelector(textDataErrorSelector);

  return (
    <Card bordered={false}>
      <Paragraph>
        {status && <Spin style={{ width: '100%', textAlign: 'center' }} />}
        <span style={{ color: '#3f8600' }}>{enteredText}</span>
        <span
          style={{
            backgroundColor: error ? 'red' : '#3f8600',
            color: 'white',
          }}
        >
          {firstLetter}
        </span>
        {othersLetters.join('')}
      </Paragraph>
    </Card>
  );
};
