import { useSelector } from 'react-redux';
import { startTimeSelector } from '../../selectors/userInputSelectors';
import { useEffect, useState } from 'react';
import { textDataEnteredTextSelector } from '../../selectors/textDataSelector';
import { Card, Col, Statistic } from 'antd';

const useGetSpeed = () => {
  const [speed, setSpeed] = useState(0);

  const count = useSelector(textDataEnteredTextSelector).length;
  const startTime = useSelector(startTimeSelector);

  useEffect(() => {
    const interval = setInterval(() => {
      const now = new Date().getTime();
      const time = (now - startTime) / 60000;
      setSpeed(count / time);
    }, 300);

    return () => {
      clearInterval(interval);
    };
  }, [count, startTime]);

  return Math.floor(speed);
};

export const Speed = () => {
  const speed = useGetSpeed();

  return (
    <Col span={12}>
      <Card bordered={false} style={{ marginTop: 16 }}>
        <Statistic
          title='Скорость'
          value={speed}
          precision={2}
          valueStyle={{ color: '#3f8600' }}
          suffix='зн./мин.'
        />
      </Card>
    </Col>
  );
};
