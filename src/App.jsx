import Layout, { Footer, Header } from 'antd/es/layout/layout';
import { Text } from './components/Text/Text';
import { Accurancy } from './components/Accurancy/Accurancy';
import { Speed } from './components/Speed/Speed';
import { Col, Row } from 'antd';
import './App.css';
import { Reset } from './components/Reset/Reset';

export const App = () => {
  return (
    <Layout className='wrap'>
      <Header />
      <Layout className='container'>
        <Row justify='space-between'>
          <Col span={16}>
            <Text />
          </Col>
          <Col span={6} align='end'>
            <Accurancy />
            <Speed />
            <Reset />
          </Col>
        </Row>
      </Layout>
      <Footer style={{ textAlign: 'center' }}>Typing Test ©2023</Footer>
    </Layout>
  );
};
