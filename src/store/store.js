import { configureStore, combineReducers } from '@reduxjs/toolkit';
import textDataSlice from '../slices/textDataSlice/textDataSlice';
import userInputSlice from '../slices/userInputSlice/userInputSlice';

const rootReducer = combineReducers({
  text: textDataSlice,
  userInput: userInputSlice,
});

export const store = configureStore({
  reducer: rootReducer,
});
